﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mojp2
{
    public partial class AboutBox : Form
    {
        static AboutBox newMessageBox;
        public AboutBox()
        {
            InitializeComponent();
        }

        public static void ShowBox()
        {
            newMessageBox = new AboutBox();
            newMessageBox.ShowDialog();
        }

        private void btn_OK_Click(object sender, EventArgs e)
        {
            newMessageBox.Dispose();
        } 
    }
}
