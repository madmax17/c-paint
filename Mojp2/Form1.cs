﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using Microsoft.VisualBasic;
using System.IO;


namespace Mojp2
{

    public partial class Form1 : Form
    {
        Point startPoint = new Point();
        Point currentPoint = new Point();
        Point endPoint = new Point();
        Color color=Color.Black;            //Default Color is black
        float PenWidth = 1;                 //Default pen width
        bool mouse_Down = false;
        Font drawFont = new Font("Arial", 16);
        int width, height;
        String drawString = "";

        Point p1 = new Point();
        Point p2 = new Point();

        Rectangle rect;
        Rectangle ellipse;

        List<GraphicsObj> gObj = new List<GraphicsObj>();

        bool CanPaint = false;
        bool clear = false;

        enum ToolSelect   
	        {//  0      1       2         3        4
	            Nista, Line, Rectangle, String, Ellipse
	        };
	             
	    ToolSelect tool;     

        public Form1()
        {
            InitializeComponent();

            //this.DoubleBuffered = true;

            //Set Double Buffering
            pictureBox1.GetType().GetMethod("SetStyle",
              System.Reflection.BindingFlags.Instance |
              System.Reflection.BindingFlags.NonPublic).Invoke(pictureBox1,
              new object[]{ System.Windows.Forms.ControlStyles.UserPaint | 
	         System.Windows.Forms.ControlStyles.AllPaintingInWmPaint | 
	         System.Windows.Forms.ControlStyles.DoubleBuffer, true });
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            width=pictureBox1.Width;
            height=pictureBox1.Height;        
        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        { 
            startPoint.X = e.X;
            startPoint.Y = e.Y;
            mouse_Down = true;
            pictureBox1.Invalidate();
            //MessageBox.Show(pictureBox1.Width + " " + pictureBox1.Height +
            //" " + pictureBox1.Location.X + " " + pictureBox1.Location.Y);
        }

        private void pictureBox1_MouseClick(object sender, MouseEventArgs e)
        {
            if (mouse_Down && CanPaint)
            {
                switch (tool)
                {
                    case ToolSelect.String:
                        string s;
                        s = Interaction.InputBox("Enter text: ", " ", "", 320, 220);
                        if (s == "")                        //if cancel button is pressed
                        {
                            tool = ToolSelect.Nista;
                            toolBtnString.Checked = false;
                            mouse_Down = false;
                        }
                        drawString = s;
                        SolidBrush drawBrush = new SolidBrush(color);
                        p1=new Point(startPoint.X, startPoint.Y);
                        mouse_Down = false;
                        break;
                }
            }
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            currentPoint.X = e.X;
            currentPoint.Y = e.Y;
            statusStrip.Items[1].Text = "(" + e.X.ToString() + "," + e.Y.ToString() + ")";

            statusStrip.Items[2].Text = gObj.Count.ToString();

            if (e.Button == MouseButtons.Left)                //limit mouse movement to picturebox1
            {
                Size l= new Size(65,12);
                Size s = pictureBox1.Size + l;
                Point p = new Point(65, 12);
                //Point r = PointToScreen(Point.Empty) + p;                  
                Cursor.Clip = new Rectangle(PointToScreen(Point.Empty), s);    //new Rectangle(r,s);
            }

            if (mouse_Down && CanPaint)
            {
                switch (tool)
                {
                    case ToolSelect.Line:
                        p1=new Point(startPoint.X, startPoint.Y);
                        p2=new Point(currentPoint.X, currentPoint.Y);
                        break;

                    case ToolSelect.Rectangle:
                        rect = new Rectangle(startPoint.X, startPoint.Y, e.X - startPoint.X, e.Y - startPoint.Y);
                        break;

                    case ToolSelect.Ellipse:
                        ellipse = new Rectangle(startPoint.X, startPoint.Y, e.X - startPoint.X, e.Y - startPoint.Y);
                        break;
                }
            }
          pictureBox1.Invalidate();
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)   //send object drawn into the list
        {
            endPoint.X = e.X;
            endPoint.Y = e.Y;
            mouse_Down = false;
            Pen Pn = new Pen(color, PenWidth);
            SolidBrush drawBrush = new SolidBrush(color);

            Cursor.Clip = Rectangle.Empty;

            switch (tool)
            {
                case ToolSelect.Line:
                    p1 = new Point(startPoint.X, startPoint.Y);
                    p2 = new Point(currentPoint.X, currentPoint.Y);
                    cLine cl = new cLine(Pn, p1, p2);
                    gObj.Add(cl);
                    break;

                case ToolSelect.Rectangle:
                    rect = new Rectangle(startPoint.X, startPoint.Y, currentPoint.X - startPoint.X, 
                        currentPoint.Y - startPoint.Y);
                    cRectangle cr= new cRectangle(Pn, rect);
                    gObj.Add(cr);
                    break;

                case ToolSelect.Ellipse:
                    ellipse = new Rectangle(startPoint.X, startPoint.Y, currentPoint.X - startPoint.X,
                        currentPoint.Y - startPoint.Y);
                    cEllipse ce = new cEllipse(Pn, ellipse);
                    gObj.Add(ce);
                    break;

                case ToolSelect.String:
                    cString cs = new cString(drawString, drawFont, drawBrush, p1);
                    gObj.Add(cs);
                    break;
            }
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {       
           //Apply a smoothing mode to smooth out the line.
           e.Graphics.SmoothingMode = SmoothingMode.HighQuality;
           e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
           Pen Pn = new Pen(color, PenWidth);
           SolidBrush drawBrush = new SolidBrush(color);

           switch (tool)
           {
               case ToolSelect.Line:
                   e.Graphics.DrawLine(Pn,p1,p2);
                   break;

               case ToolSelect.Rectangle:
                   e.Graphics.DrawRectangle(Pn, rect);
                   break;

               case ToolSelect.String:
                   e.Graphics.DrawString(drawString, drawFont, drawBrush, p1);
                   break;

               case ToolSelect.Ellipse:
                   e.Graphics.DrawEllipse(Pn, ellipse);
                   break;
           }

            foreach (GraphicsObj el in gObj){                   //draw from list
                el.Draw(e.Graphics);
            }

           if (clear)
           {
               //e.Graphics.Clear(pictureBox1.BackColor);
               clear = false;
           }

        }

        private void btn_Color_Click_1(object sender, EventArgs e)
        {
            //Show and Get the result of the colour dialog
            DialogResult D = colorDialog1.ShowDialog();
            if (D == DialogResult.OK)
            {
                color = colorDialog1.Color;
            }
        }

        private void toolBtnLine_Click(object sender, EventArgs e)
        {
            if (tool != ToolSelect.Line)
            {
                tool = ToolSelect.Line;
                toolBtnRect.Checked = false;
                toolBtnString.Checked = false;
                toolBtnEllipse.Checked = false;
            }
            else
            {
                tool = ToolSelect.Nista;
                toolBtnLine.Checked = false;
            }
        }

        private void toolBtnRect_Click(object sender, EventArgs e)
        {
            if (tool != ToolSelect.Rectangle)
            {
                tool = ToolSelect.Rectangle;
                toolBtnLine.Checked = false;
                toolBtnString.Checked = false;
                toolBtnEllipse.Checked = false;
            }
            else
            {
                tool = ToolSelect.Nista;
                toolBtnRect.Checked = false;
            }
        }

        private void pictureBox1_MouseEnter(object sender, EventArgs e)
        {
            this.Cursor = System.Windows.Forms.Cursors.Cross;
            CanPaint = true;
        }

        private void pictureBox1_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = System.Windows.Forms.Cursors.Default;
            CanPaint = false;
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            gObj.Clear();
            
            pictureBox1.Invalidate();
            //pictureBox1_Paint(sender,pnt_event);

            //clear_sc();

        }

        private void toolBtnString_Click(object sender, EventArgs e)
        {
            if (tool != ToolSelect.String)
            {
                tool = ToolSelect.String;
                toolBtnLine.Checked = false;
                toolBtnRect.Checked = false;
                toolBtnEllipse.Checked = false;
            }
            else
            {
                tool = ToolSelect.Nista;
                toolBtnString.Checked = false;
            }
        }

        private void btnFont_Click(object sender, EventArgs e)
        {
            DialogResult D = fontDialog1.ShowDialog();
            if (D == DialogResult.OK)
            {
                drawFont = fontDialog1.Font;
            }
        }

        private void toolBtnEllipse_Click_1(object sender, EventArgs e)
        {
            if (tool != ToolSelect.Ellipse)
            {
                tool = ToolSelect.Ellipse;
                toolBtnLine.Checked = false;
                toolBtnRect.Checked = false;
                toolBtnString.Checked = false;
            }
            else
            {
                tool = ToolSelect.Nista;
                toolBtnEllipse.Checked = false;
            }
        }

        private void numericUpDown1_ValueChanged_1(object sender, EventArgs e)
        {
            PenWidth = Convert.ToSingle(numericUpDown1.Value);
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            statusStrip.Items[0].Text = tool.ToString();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "Jura Files (*.jur)|*.jur";
            saveFileDialog1.Title = "Save a Jura File";
            saveFileDialog1.OverwritePrompt = true;

            if (gObj.Count == 0)
            {
                MessageBox.Show("Can't save 0 objects, paint something", "Error",MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
            }
            else
            {
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    //StreamWriter myStream = new StreamWriter(saveFileDialog1.FileName, true);
                    //FileStream fs = new FileStream();
                    {
                        foreach(GraphicsObj el in gObj)
                        {
                            //fs.Write(el, 0, 4);

                          // fs.BeginWrite(
                        }
                    }

                   // fs.Close();
                }
                
            }
             
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox.ShowBox();
        }

        public void clear_sc()
        {
            Graphics s = pictureBox1.CreateGraphics();
            s.Clear(pictureBox1.BackColor);
        }
    }

        public class GraphicsObj
        {
            public Point p1 = new Point();
            public Point p2 = new Point();
            public static Color color;
            public static float PenWidth;
            public Pen Pn = new Pen(color, PenWidth);
            public Rectangle rect = new Rectangle();

            public String st;
            public Font dFont = new Font("Arial", 16);
            public SolidBrush dBrush = new SolidBrush(color);

            public virtual void Draw(Graphics g) {}
        }

        class cLine : GraphicsObj
        {
            public cLine(Pen p, Point po1, Point po2)
            {
                Pn = p;
                p1 = po1;
                p2 = po2;
            }

            public override void Draw(Graphics g)
            {
                g.DrawLine(Pn, p1, p2);
            }
        }

        class cRectangle : GraphicsObj
        {
            public cRectangle(Pen p, Rectangle r)
            {
                Pn = p;
                rect = r;
            }

            public override void Draw(Graphics g)
            {
                g.DrawRectangle(Pn, rect);
            }

        }

        class cString : GraphicsObj
        {
            public cString(String s, Font f, SolidBrush b, Point po1)
            {
                st = s;
                dFont = f;
                dBrush=b;
                p1=po1;
            }

            public override void  Draw(Graphics g)
            {
 	             g.DrawString(st, dFont, dBrush, p1);
            }  
        }

        class cEllipse : GraphicsObj
        {
            public cEllipse(Pen p, Rectangle r)
            {
                Pn = p;
                rect = r;
            }

            public override void Draw(Graphics g)
            {
                g.DrawEllipse(Pn, rect);
            }
        }
}